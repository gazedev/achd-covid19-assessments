# ACHD COVID-19 Assessments

A little project to grab data from the Allegheny County COVID-19 Assessment Dashboard
and collect the information onto one page. It uses a small node script to control
a chrome browser to go through all the entries on the page, request their reports
in HTML format, collect all the information in JSON format. A static site generator
(Eleventy/11ty) then displays the information from the JSON. This process should
be able to be put on a job to keep the information up to date.
