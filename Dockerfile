# Node v12
FROM node:12.18.2

# Create app directory
WORKDIR /usr/src/app/node
COPY node/package.json node/package-lock.json ./
RUN npm install
COPY ./node .

WORKDIR /usr/src/app/site
COPY site/package.json site/package-lock.json ./
RUN npm install
COPY ./site .

WORKDIR /usr/src/app
