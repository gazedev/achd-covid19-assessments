module.exports = function(eleventyConfig) {
  eleventyConfig.addPassthroughCopy('www/assets');
  eleventyConfig.addPassthroughCopy('www/css');
  eleventyConfig.addPassthroughCopy('www/_data/covid19Assessments.json');

  eleventyConfig.addFilter("localDate", function(value) {
    let date = new Date(value);
    return date.toLocaleString("en-US", {timeZone: "America/New_York"});;
  });

  return {
    passthroughFileCopy: true,
    dir: {
      input: "www",
      output: "dist",
      layouts: "/_layouts",
    }
  }
}
