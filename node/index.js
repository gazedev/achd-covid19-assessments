'use strict'

console.log('Starting...');
const models = require('./models/index.js');
const sequelize = models.sequelize;
(async function() {

  // establish db connection

  let retries = 10;
  let connected = await checkDbConnection(sequelize, retries);
  console.log('connected', connected);
  if (connected) {
    console.log('connection established');
  } else {
    console.log('Connection could not be established after ', retries, ' retries');
    process.exit(1);
  }

  const CDP = require('chrome-remote-interface');
  const cdpOptions = {
    host: 'chrome'
  };

  let target = {};
  try {
    target = await CDP.New(cdpOptions);
    const Client = await CDP({target: target});
    const {Page} = Client;
    await Page.enable();
    await Page.navigate({url: 'https://eapps.alleghenycounty.us/cFips/cDashBoard.aspx'});
    await Page.loadEventFired();
    let results = [];
    try {
      //  Loop through the main table and build
      let getMainTableData = [
        `
        let results = [];
        let tableRows = document.querySelectorAll('#divData table tbody tr');
        // splits the rows into headerRow and bodyRows;
        // see: https://stackoverflow.com/a/51196769/2383249
        let [headerRow, ...bodyRows] = tableRows;
        let tableHeaderDatas = headerRow.querySelectorAll('th');
        let headerValues = [];
        for (let row of tableHeaderDatas) {
          let header = row.innerText;
          headerValues.push(header);
        }
        // removes the header row from the array
        for (let row of bodyRows) {
          let datas = row.querySelectorAll('td');
          let dataObj = {};
          for (let i=0; i < datas.length; i++) {
            let key = headerValues[i];
            let data;
            if (key == "Report") {
              data = datas[i].querySelectorAll('a')[0].href;
              if (datas[i].classList.contains('btn-success')) {
                dataObj['Satisfactory'] = true;
              } else {
                dataObj['Satisfactory'] = false;
              }
            } else {
              data = datas[i].innerText;
            }
            dataObj[key] = data;
          }
          results.push(dataObj);
        }
        return JSON.stringify(results);
        `
      ];
      let mainTableData = await executeCommands(Client,getMainTableData);
      let numberOfResults = mainTableData.length;
      for (let i = 0; i < numberOfResults; i++) {
        let encounterId = mainTableData[i]['Encounter'];
        let assessment;
        try {
          assessment = await findAssesment(encounterId);
        } catch (e) {
          console.error(`Error with findAssessment(${encounterId})`, e);
          assessment = null;
        }

        if (assessment === null) {
          try {
            assessment = mainTableData[i];
            let reportUrl = assessment['Report'];
            console.log(`Collecting: report ${i} of ${numberOfResults}...`);
            let reportXML = await collectReportXML(reportUrl);
            let reportData = await collectReportDataViaXML(reportXML);
            assessment['Report Data'] = extractAndNormalizeData(reportData);
            await createAssessment(assessment, reportXML);
          } catch (e) {
            console.log('Error creating Assessment for Encounter', encounterId);
          }
        } else {
          assessment = assessment.get({
            plain: true,
          });
          console.log(`Found in DB: report ${i} of ${numberOfResults}...`);
        }
        results.push(assessment);
      }
    } catch (e) {
      console.error('catch', e);
      throw 'Error during command execution';
    }
    await CDP.Close({ ...cdpOptions, ...{id: target.id}});
    console.log('Finished collecting results. Writing to file...')
    await writeDataToFile(results);
  } catch (err) {
    console.error(err);
    if (target.hasOwnProperty('id')) {
      try {
        await CDP.Close({ ...cdpOptions, ...{id: target.id}});
      } catch (e) {
        console.error('error closing in finally', e);
      }
    }
    throw 'Error during opening or closing page';
  }
  console.log('Complete!');
})();

async function checkDbConnection(sequelize, retries = 0) {
  console.log('on retry', retries);
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
    return true;
  } catch (error) {
    console.error('Unable to connect to the database:', error);
    if (retries > 0) {
      await wait(2);
      return checkDbConnection(sequelize, retries-1);
    } else {
      return false;
    }
  }
}

async function wait(seconds) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(true)
    }, seconds * 1000);
  });
}

async function executeCommands(Client, commands) {
  const {Page, Runtime} = Client;
  try {
    let result;
    for (let command of commands) {
      if (command == 'wait') {
        await Page.loadEventFired();
      } else {
        // we encapsulate commands so the console doesn't error on looped uses
        // of let and const
        result = await Runtime.evaluate({expression: `(()=>{
          ${command}
        })();`});
      }
    }
    if (result.result.type == 'undefined') {
      return null;
    }
    return JSON.parse(result.result.value);
  } catch (e) {
    console.error('error executing command', e);
    return null;
  }
}

async function collectReportXML(reportUrl) {
  let xml;
  try {
    reportUrl = reportUrl.replace('desformat=PDF', 'desformat=XML');
    xml = await getPageXML(reportUrl);
  } catch (e) {
    console.log('error getting page xml', e);
    process.exit(1);
  }
  return xml;
}

async function collectReportDataViaXML(xml) {
  try {
    const xml2js = require('xml2js');
    var parser = new xml2js.Parser(/* options */);
    let json = parser.parseStringPromise(xml);
    return json;
  } catch (e) {
    console.log('error parsing page xml', e);
    process.exit(1);
  }
}

async function getPageXML(url) {
  let https = require('http');
  return new Promise((resolve, reject) => {
    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        resolve(body);
      });
      res.on('error', error => {
        reject(error);
      });
    });
  });
}

function extractAndNormalizeData(entry) {
  entry = entry.INSP_SUMMARY_COVID;
  let response = {};
  let g1 = entry.LIST_G_1[0].G_1[0];
  response['Client ID'] = g1.ID[0];
  response['Client Name'] = g1.CLIENT_NAME[0];
  response['Address'] = g1.ST_NAME[0];
  response['Date'] = g1.SYS_DATE[0];
  response['City'] = g1.CITY[0];
  response['State'] = g1.STATE[0];
  response['Zip'] = g1.ZIP[0];
  response['Municipality'] = g1.MUNICIPALITY[0];
  response['Assessor'] = g1.CF_INSP_NUM[0];
  response['Category Code'] = g1.C_CODE[0];
  response['Assessments'] = extractAssessments(entry);
  response['Details'] = extractDetails(entry);
  response['Satisfactory Comments'] = extractSatisfactoryComments(entry);
  response['Unsatisfactory Comments'] = extractUnsatisfactoryComments(entry);
  response['General Comments'] = extractGeneralComments(entry);
  return response;
}

function extractAssessments(entry) {
  let asmts = entry.LIST_G_CRITICAL[0].G_CRITICAL[0].LIST_G_DESCRIPTION_NEW1[0].G_DESCRIPTION_NEW1;
  let response = {};
  for (let item of asmts) {
    let assessment = item.DESCRIPTION_NEW1[0];
    response[assessment] = {
      'Satisfactory': (item.RATING2[0] == 'Satisfactory'),
      'Unsatisfactory': (item.RATING2[0] == 'Violation'),
      'Not Rated': (item.RATING2[0] == 'Not Rated'),
    };
  }
  response['Results'] = {
    'Satisfactory': entry.LIST_G_CRITICAL[0].G_CRITICAL[0].CS_SUM_SATISFY[0],
    'Unsatisfactory': entry.LIST_G_CRITICAL[0].G_CRITICAL[0].CS_SUM_VIOL[0],
  };
  return response;
}

function extractDetails(entry) {
  let g1 = entry.LIST_G_1[0].G_1[0];
  let response = {};
  response['Assessor'] = g1.CF_INSP_NUM[0];
  response['Contact'] = g1.CONTACT[0];
  // response['Start Time'] = '';
  // response['End Time'] = '';
  response['Phone'] = g1.CF_PHONE[0];
  return response;
}

function extractSatisfactoryComments(entry) {
  let comments = entry.LIST_G_1[0].G_1[0].LIST_G_NC_ENCOUNTER[0].G_NC_ENCOUNTER;
  if (!comments) {return false;}
  let response = {};
  for (let comment of comments) {
    let topic = comment.NC_LONG_DESC[0];
    response[topic] = comment.NC_COMMENTS[0];
  }
  return response;
}

function extractUnsatisfactoryComments(entry) {
  let comments = entry.LIST_G_1[0].G_1[0].LIST_G_VIOLATION_CD1[0].G_VIOLATION_CD1;
  if (!comments) {return false;}
  let response = {};
  for (let comment of comments) {
    let topic = comment.LONGDESC1[0];
    response[topic] = comment.CF_V_CMT[0];
  }
  return response;
}

function extractGeneralComments(entry) {
  let response = {};
  response['Comments'] = entry.LIST_G_1[0].G_1[0].CF_GEN_CMT[0];
  return response;
}

function writeDataToFile(data) {
  let fs = require('fs');
  let writer = fs.createWriteStream('../site/www/_data/covid19Assessments.json');
  let meta = fs.createWriteStream('../site/www/_data/metadata.json');
  return Promise.all([
    new Promise((resolve, reject) => {
      writer.write(JSON.stringify(data), () => {
        resolve(true);
      });
    }),
    new Promise((resolve, reject) => {
      let utc = new Date().toUTCString();
      meta.write(JSON.stringify({lastUpdated: utc}), () => {
        resolve(true);
      });
    })
  ]);
}

function findAssesment(encounterId) {
  return models.Assessment.findOne({
    where: {
      encounter: encounterId,
    },
    attributes: [
      // we leave out id and reportXML and remap field names
      ['name', 'Facility Name'],
      ['inspectionDate', 'Insp Date'],
      ['address', 'Address'],
      ['zip', 'Zip'],
      ['municipality', 'Municipality'],
      ['purpose', 'Purpose'],
      ['encounter', 'Encounter'],
      ['satisfactory', 'Satisfactory'],
      ['report', 'Report'],
      ['reportData', 'Report Data'],
    ],
  });
}

function createAssessment(assessment, xml) {
  let data = {
    name: assessment['Facility Name'],
    inspectionDate: assessment['Insp Date'],
    address: assessment['Address'],
    zip: assessment['Zip'],
    municipality: assessment['Municipality'],
    purpose: assessment['Purpose'],
    encounter: assessment['Encounter'],
    satisfactory: assessment['Satisfactory'],
    report: assessment['Report'],
    reportXML: xml,
    reportData: assessment['Report Data'],
  };
  return models.Assessment.create(data);
}
