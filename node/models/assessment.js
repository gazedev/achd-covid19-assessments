'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Assessment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Assessment.init({
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    name: DataTypes.STRING,
    inspectionDate: DataTypes.STRING,
    address: DataTypes.STRING,
    zip: DataTypes.STRING,
    municipality: DataTypes.STRING,
    purpose: DataTypes.STRING,
    encounter: DataTypes.STRING,
    satisfactory: DataTypes.BOOLEAN,
    report: DataTypes.TEXT,
    reportXML: DataTypes.TEXT,
    reportData: DataTypes.JSONB
  }, {
    sequelize,
    modelName: 'Assessment',
  });
  return Assessment;
};
