'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Assessments', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
      },
      name: {
        type: Sequelize.STRING
      },
      inspectionDate: {
        type: Sequelize.STRING
      },
      address: {
        type: Sequelize.STRING
      },
      zip: {
        type: Sequelize.STRING
      },
      municipality: {
        type: Sequelize.STRING
      },
      purpose: {
        type: Sequelize.STRING
      },
      encounter: {
        type: Sequelize.STRING
      },
      satisfactory: {
        type: Sequelize.BOOLEAN
      },
      report: {
        type: Sequelize.TEXT
      },
      reportXML: {
        type: Sequelize.TEXT
      },
      reportData: {
        type: Sequelize.JSONB
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Assessments');
  }
};
