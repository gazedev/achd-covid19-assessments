'use strict'

async function collectReportData(Client, reportUrl) {
  reportUrl = reportUrl.replace('desformat=PDF', 'desformat=HTML');
  const {Page} = Client;
  await Page.enable();
  await Page.navigate({url: reportUrl});
  await Page.loadEventFired();
  let results = {};
  try {
    // move through the array backwards removing any tabs only lines
    // move backwards so yet to iterate indicies continue to match loop index
    let getTbodyText = [
      `
      let results = {};
      let tableBody = document.querySelectorAll('table tbody');
      let tbodyText = tableBody[0].innerText;
      results = tbodyText;
      return JSON.stringify(results);
      `,
    ];
    let tbodyText = await executeCommands(Client, getTbodyText);

    let textRows = tbodyText.split(/\r?\n/g);
    for (let i = textRows.length-1; i >= 0; i--) {
      let row = textRows[i];
      let preTabs = /^(\t)+/;
      if (row.replace(preTabs, '') == "") {
        textRows.splice(i, 1);
        continue;
      }
    }

    let sections = generateSectionsForTextRows(textRows);

    for (let i = 0; i < textRows.length; i++) {
      let row = textRows[i];
      if (inSection(i, 'info', sections)) {
        if (-1 < row.indexOf('Client ID')) {
          let pieces = row.split('\t\t');
          results['Client ID'] = pieces[1].replace(/\t/g, '');
          results['Client Name'] = pieces[3].replace(/\t/g, '');
        }
        else if (-1 < row.indexOf('Address')) {
          let pieces = row.split('\t\t');
          results['Address'] = pieces[1].replace(/\t/g, '');
          results['Date'] = pieces[2].replace(/\t/g, '');
        }
        else if (-1 < row.indexOf('City')) {
          let pieces = row.split('\t\t');
          results['City'] = pieces[1].replace(/\t/g, '');
          results['State'] = pieces[3].replace(/\t/g, '');
          results['Zip'] = pieces[5].replace(/\t/g, '');
        }
        else if (-1 < row.indexOf('Municipality')) {
          let pieces = row.split('\t\t');
          results['Municipality'] = pieces[1].replace(/\t/g, '');
          results['Assessor'] = pieces[3].replace(/\t/g, '');
        }
        else if (-1 < row.indexOf('Category Code')) {
          let pieces = row.split('\t\t');
          results['Category Code'] = pieces[1].replace(/\t/g, '');
        }
      }
      if (inSection(i, 'assessments', sections)) {
        if (!results.hasOwnProperty('Assessments')) {
          results['Assessments'] = {};
        }
        if (i <= sections['assessments'][0]+1) {
          // We're still on a header row
        }
        else if (i >= sections['assessments'][1]-1) {
          // We're in the results section
          let pieces = row.split('\t\t');
          results['Assessments']['Results'] = {
            'Satisfactory': pieces[0].replace(/\t/g, ''),
            'Unsatisfactory': pieces[1].replace(/\t/g, ''),
          };
        }
        else if (-1 < row.indexOf('Indoor occupancy')) {
          // Indoor occupancy marks are on the next line for some reason
          let nextRow = textRows[i+1];
          let combined = row.replace(/\t/g, '') + nextRow;
          let pieces = combined.split('\t\t');
          let label = pieces[0].replace(/\t/g, '');
          let isSatis = (pieces[1].replace(/\t/g, '') == 'x');
          let isUnsatis = (pieces[2].replace(/\t/g, '') == 'x');
          results['Assessments'][label] = {
            'Satisfactory': isSatis,
            'Unsatisfactory': isUnsatis,
          };
          // advance the counter by 1 to skip the next line
          i++;
        }
        else {
          // other lines are normally formatted assessments (hopefully)
          let pieces = row.split('\t\t');
          let label = pieces[0].replace(/\t/g, '');
          let isSatis = (pieces[1].replace(/\t/g, '') == 'x');
          let isUnsatis = (pieces[2].replace(/\t/g, '') == 'x');
          results['Assessments'][label] = {
            'Satisfactory': isSatis,
            'Unsatisfactory': isUnsatis,
          };
        }
      }
      if (inSection(i, 'details', sections)) {
        if (!results.hasOwnProperty('Details')) {
          results['Details'] = {};
        }
        if (i == sections['assessments'][0]) {
          // We're still on a header row
        }
        else if (-1 < row.indexOf('Assessor')) {
          let pieces = row.split('\t\t');
          results['Details']['Assessor'] = pieces[1].replace(/\t/g, '');
          results['Details']['Assessor'] = pieces[3].replace(/\t/g, '');
        }
        else if (-1 < row.indexOf('Start Time')) {
          let pieces = row.split('\t\t');
          results['Details']['Start Time'] = pieces[1].replace(/\t/g, '');
          results['Details']['End Time'] = pieces[3].replace(/\t/g, '');
          results['Details']['Phone'] = pieces[5].replace(/\t/g, '');
        }
        // TODO: record the following in a useful way
        // remaining rows in this section are probably comments about unsatisfactory findings like below:
        // '\tUnsatisfactory\t\tFace coverings by staff\t',
        // '\tComments:\t\tWearing mask under nose, advised of proper usage\t',
      }
      if (inSection(i, 'observations', sections)) {
        if (i == sections['observations'][0]) {
          // We're still on a header row
        }
        // TODO: record the following in a useful way
        // rows in this section are probably comments about satisfactory findings like below:
        // '\t \t\tClosed bar seating\t\tSatisfactory\t',
        // '\tn/a\t',
      }
    }

  } catch (e) {
    console.log('catch', e);
    throw 'Error during collectReportData command execution';
  }
  return results;
}

function generateSectionsForTextRows(textRows) {
  let sections = {};
  let waypoints = [
    {
      text: 'Allegheny County Health Department',
      sectionKey: 'letterhead',
    },
    {
      text: 'Client ID',
      sectionKey: 'info',
    },
    {
      text: 'Assessment Categories',
      sectionKey: 'assessments',
    },
    {
      text: 'Assessment Details',
      sectionKey: 'details',
    },
    {
      text: 'Other assessment',
      sectionKey: 'observations',
    }
  ];
  let currentWaypoint = 0;

  // create sections from waypoints
  for (let i = 0; i < textRows.length; i++) {
    let textToFind = waypoints[currentWaypoint].text;
    let row = textRows[i];
    if (-1 < row.indexOf(textToFind) && row.indexOf(textToFind) < 7) {
      let wpSection = waypoints[currentWaypoint].sectionKey;
      sections = demarcate(i, wpSection, sections);
      if (currentWaypoint+1 < waypoints.length) {
        currentWaypoint++;
      }
    }
    if (i == textRows.length-1) {
      // we're in the last row, make demarcate the last section
      sections = demarcate(i+1, '', sections);
    }
  }
  return sections;
}

function demarcate(rownum, section, sections) {
  // We are building an object like so, where startNum and endNum are the text
  // rows that mark the starts and ends of a section
  // {
  //   sectionName: [startNum, endNum],
  // }
  let keys = Object.keys(sections);
  if (keys.length !== 0) {
    // set the end point for the previous section to be where we are minus 1
    let previousSection = keys[keys.length-1];
    sections[previousSection].push(rownum-1);
  }
  if (section !== '') {
    sections[section] = [rownum];
  }

  return sections;
}

function inSection(rownum, section, sections) {
  let span = sections[section];
  let start = span[0];
  let end = span[1];
  return start <= rownum && rownum <= end;
}
